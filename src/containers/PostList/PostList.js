import React, {Component} from 'react';

import {Button, Card, CardBody, CardTitle, Col, Row} from "reactstrap";

import axios from '../../axios-blog';


class PostList extends Component {
    state = {
        posts: null
    };

    componentDidMount() {
        axios.get('posts.json').then(response => {
            const posts = Object.keys(response.data).map(id => {
                return {...response.data[id], id};
            });

            this.setState({posts})
        })
    }

    readMore = (id) => {
        this.props.history.push(`/posts/${id}`);
    };

    render() {
        let posts = null;

        if (this.state.posts) {
            posts = this.state.posts.map((post, index) => (
                <Card key={index} inverse style={{ margin: "20px 0 0 140px", width: "50em"}}>
                    <CardBody>
                        <CardTitle style={{color: '#000'}}>{post.title}</CardTitle>
                        <Button onClick={() => this.readMore(post.id)}>Read More »</Button>
                    </CardBody>
                </Card>
            ));
        }

        return (
            <Row style={{marginTop: "20px"}}>
                <Col sm={3}>
                    <h1>Posts:</h1>
                    {posts}
                </Col>
            </Row>
        );
    }
}

export default PostList;
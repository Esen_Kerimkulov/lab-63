import React, { Component } from 'react';
import {Container, Jumbotron, ListGroup, ListGroupItem, ListGroupItemHeading, ListGroupItemText} from "reactstrap";


class Contact extends Component {
    render() {
        return (
            <div>
                <Jumbotron fluid >
                    <Container fluid>
                <ListGroup style={{margin:' 0 70px 0 70px'}}>
                    <ListGroupItem color="dark" >
                        <ListGroupItemHeading style={{fontSize: '40px'}} >Corporate Contacts</ListGroupItemHeading>
                    </ListGroupItem>
                    <ListGroupItem>
                        <ListGroupItemHeading><b>United States</b></ListGroupItemHeading>
                        <ListGroupItemText>
                            Media Helpline (408) 974–2042
                        </ListGroupItemText>
                        <ListGroupItemText>
                        Software Upgrade Center (888) 840–8433
                        </ListGroupItemText>
                        <ListGroupItemText>
                        Reseller Referral (Resellers, Trainers, Consultants) (800) 538–9696
                        </ListGroupItemText>
                    </ListGroupItem>
                    <ListGroupItem>
                        <ListGroupItemHeading><b>Canada</b></ListGroupItemHeading>
                        <ListGroupItemText>
                            Store (Consumer and Education Individuals) (800) MY–APPLE (800–692–7753)
                        </ListGroupItemText>
                        <ListGroupItemText>
                        Store (Small Business) 001–800–692–7753
                        </ListGroupItemText>
                    </ListGroupItem>
                </ListGroup>
                    </Container>
                </Jumbotron>
            </div>
        );
    }
}

export default Contact;
import React, { Component } from 'react';
import {Container, Jumbotron} from "reactstrap";


class About extends Component {
    render() {
        return (
            <div>
              <Jumbotron fluid style={{padding: '60px'}}>
                <Container fluid>
                    <h1 className="display-3">About Us</h1>
                        <p className="lead">Pinterest semiotics single-origin coffee craft beer
                            thundercats irony, tumblr bushwick intelligentsia pickled.
                            Narwhal mustache godard master cleanse street art, occupy
                            ugh selfies put a bird on it cray salvia four loko
                            gluten-free shoreditch. Occupy american apparel freegan
                            cliche. Mustache trust fund 8-bit jean shorts mumblecore
                            thundercats. Pour-over small batch forage cray, banjo
                            post-ironic flannel keffiyeh cred ethnic semiotics next
                            level tousled fashion axe. Sustainable cardigan keytar fap
                            bushwick bespoke.<br/><br/>
                            Narwhal mustache godard master cleanse street art, occupy
                            ugh selfies put a bird on it cray salvia four loko
                            gluten-free shoreditch. Occupy american apparel freegan
                            cliche. Mustache trust fund 8-bit jean shorts mumblecore
                            thundercats. Pour-over small batch forage cray, banjo
                            post-ironic flannel keffiyeh cred ethnic semiotics next
                            level tousled fashion axe. Sustainable cardigan keytar fap
                            bushwick bespoke.</p>
                 </Container>
              </Jumbotron>
            </div>
        );
    }
}

export default About;